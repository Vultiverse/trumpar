﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetPosition : MonoBehaviour
{

    private Transform TrumpTransform;
    private PlayerController ButtonClick;


    void Awake()
    {
        TrumpTransform = GetComponent<Transform>();
        ButtonClick = new PlayerController();
    }


    private void OnEnable()
    {
        ButtonClick.Enable();
    }

    private void OnDisable()
    {
        ButtonClick.Disable();
    }


    void Update()
    {
        
    }
}
