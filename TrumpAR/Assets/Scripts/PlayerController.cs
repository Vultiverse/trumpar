// GENERATED AUTOMATICALLY FROM 'Assets/Scripts/PlayerController.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @PlayerController : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @PlayerController()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerController"",
    ""maps"": [
        {
            ""name"": ""PlayerMain"",
            ""id"": ""83c0141d-1dc4-4f81-bc2c-5eceb9d50c3a"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""Value"",
                    ""id"": ""bef440f9-35e5-49cd-b1b1-51c3954b8d6c"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""b7a68095-e27d-4833-ae4e-9748eb297b66"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""WASD"",
                    ""id"": ""9d80b6af-3f97-434a-a350-4137437fc800"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""7334d40a-d2ec-4e3f-9249-0d49bd06ab41"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""c45dccbe-ba2e-425b-bfc1-51fad29c31ea"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""b9c3c4ef-f37f-406f-8b52-82848ba99d7e"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""fe35cde9-695c-494a-a714-b1ba4d09e8fc"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                }
            ]
        },
        {
            ""name"": ""Reset"",
            ""id"": ""e346c3ca-b66f-440e-9633-a5058aa2767a"",
            ""actions"": [
                {
                    ""name"": ""Reset Button"",
                    ""type"": ""Button"",
                    ""id"": ""6bebb45b-d3e1-4218-82d7-fe76a1f99b4c"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""454b3dbe-6ba8-4a79-b37d-1bf9d6249783"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Reset Button"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // PlayerMain
        m_PlayerMain = asset.FindActionMap("PlayerMain", throwIfNotFound: true);
        m_PlayerMain_Move = m_PlayerMain.FindAction("Move", throwIfNotFound: true);
        // Reset
        m_Reset = asset.FindActionMap("Reset", throwIfNotFound: true);
        m_Reset_ResetButton = m_Reset.FindAction("Reset Button", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // PlayerMain
    private readonly InputActionMap m_PlayerMain;
    private IPlayerMainActions m_PlayerMainActionsCallbackInterface;
    private readonly InputAction m_PlayerMain_Move;
    public struct PlayerMainActions
    {
        private @PlayerController m_Wrapper;
        public PlayerMainActions(@PlayerController wrapper) { m_Wrapper = wrapper; }
        public InputAction @Move => m_Wrapper.m_PlayerMain_Move;
        public InputActionMap Get() { return m_Wrapper.m_PlayerMain; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerMainActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerMainActions instance)
        {
            if (m_Wrapper.m_PlayerMainActionsCallbackInterface != null)
            {
                @Move.started -= m_Wrapper.m_PlayerMainActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_PlayerMainActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_PlayerMainActionsCallbackInterface.OnMove;
            }
            m_Wrapper.m_PlayerMainActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
            }
        }
    }
    public PlayerMainActions @PlayerMain => new PlayerMainActions(this);

    // Reset
    private readonly InputActionMap m_Reset;
    private IResetActions m_ResetActionsCallbackInterface;
    private readonly InputAction m_Reset_ResetButton;
    public struct ResetActions
    {
        private @PlayerController m_Wrapper;
        public ResetActions(@PlayerController wrapper) { m_Wrapper = wrapper; }
        public InputAction @ResetButton => m_Wrapper.m_Reset_ResetButton;
        public InputActionMap Get() { return m_Wrapper.m_Reset; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(ResetActions set) { return set.Get(); }
        public void SetCallbacks(IResetActions instance)
        {
            if (m_Wrapper.m_ResetActionsCallbackInterface != null)
            {
                @ResetButton.started -= m_Wrapper.m_ResetActionsCallbackInterface.OnResetButton;
                @ResetButton.performed -= m_Wrapper.m_ResetActionsCallbackInterface.OnResetButton;
                @ResetButton.canceled -= m_Wrapper.m_ResetActionsCallbackInterface.OnResetButton;
            }
            m_Wrapper.m_ResetActionsCallbackInterface = instance;
            if (instance != null)
            {
                @ResetButton.started += instance.OnResetButton;
                @ResetButton.performed += instance.OnResetButton;
                @ResetButton.canceled += instance.OnResetButton;
            }
        }
    }
    public ResetActions @Reset => new ResetActions(this);
    public interface IPlayerMainActions
    {
        void OnMove(InputAction.CallbackContext context);
    }
    public interface IResetActions
    {
        void OnResetButton(InputAction.CallbackContext context);
    }
}
