﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour
{
    private Vector3 originalTransform;
    private Vector3 playerVelocity;
    private PlayerController playerInput;
    private CharacterController controller;
    private Animation anim;
    
    public float playerSpeed;


    private void Awake()
    {
        playerInput = new PlayerController();
        controller = GetComponent<CharacterController>();
        anim = GetComponent<Animation>();
        originalTransform = gameObject.transform.position;
    }


    private void OnEnable()
    {
        playerInput.Enable();
    }

    private void OnDisable()
    {
        playerInput.Disable();
    }

    void Update()
    {
        Vector2 movementInput = playerInput.PlayerMain.Move.ReadValue<Vector2>();
        Vector3 move = new Vector3(movementInput.x, 0.0f, movementInput.y);

        controller.Move(move * Time.deltaTime * playerSpeed);

        if (move != Vector3.zero)
        {
            gameObject.transform.forward = move;
        }

        controller.Move(playerVelocity * Time.deltaTime); 


        if (playerInput.Reset.ResetButton.triggered)
        {
            gameObject.transform.position = originalTransform;
            Debug.Log("Button Clicked");
        }

       if (gameObject.tag == "Trump")
        {
            if (movementInput.x != 0 || movementInput.y != 0)
            {
                anim.Play("walk");
            }
            else
            {
                anim.Play("idle");
            }
        }
       
        
    }
}
